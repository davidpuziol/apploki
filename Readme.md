# App Loki

A proposta do projeto é criar um app qualquer em nodejs que envia logs fake para o grafana loki.

Foi usado o nodejs e o npm padrão da instalação do ubuntu via apt-get.

```bash
sudo apt-get install nodejs npm -y
```

```bash
❯ node -v                  
v18.13.0
❯ npm -v      
9.2.0
```

As libs usadas foram:

```bash
npm install winston axios express faker
```

No Dockerfile ajuste o nível de log na variável de ambiente `LOG_LEVEL` em info ou debug.
Também é possível ajustar o endpoint que o loki estará escutando para receber os logs em `LOKI_ENDPOINT`.

Para buildar a imagem

```bash
# ajuste o repositório  repox/appname:tag
docker build -t davidpuziol/apploki .
docker push davidpuziol/apploki .
```

Um helm simples foi criado em apploki, vamos deployar no namespace demo
Ajuste o values.yaml para a sua imagem.

```bash
helm install apploki ./apploki -n demo --create-namespace
```