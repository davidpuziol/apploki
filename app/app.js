// app.js
const express = require('express');
const winston = require('winston');
const axios = require('axios');
const faker = require('faker');

const app = express();
const port = process.env.PORT || 3000;

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.Console({
      format: winston.format.simple(), // Use o formato simples no console
    }),
  ],
});

const lokiEndpoint = process.env.LOKI_ENDPOINT || 'http://localhost:3100/loki/api/v1/push';

app.get('/', (req, res) => {
  logger.info('Hello, Loki!');
  res.send('Logs sent to Loki!');
});

// Função para gerar e enviar logs aleatórios para o Loki a cada 10 segundos
function sendRandomLogs() {
  const randomLogLevel = faker.random.arrayElement(['info', 'warn', 'error']);
  const randomMessage = faker.lorem.sentence();

  logger.log({
    level: randomLogLevel,
    message: randomMessage,
  });

  sendLogsToLoki();
}

// Função para enviar logs para o Loki
async function sendLogsToLoki() {
  const logEntries = logger.transports[0].format.transform({
    level: 'info',
    message: 'Log message',
  });

  try {
    await axios.post(lokiEndpoint, {
      streams: [{
        labels: {
          job: 'node-app',
          app: 'apploki',  // Adicione este rótulo com o nome da sua aplicação
        },
        entries: logEntries,
      }],
    });
    console.log('Logs sent to Loki successfully.');
  } catch (error) {
    console.error('Error sending logs to Loki:', error.message);
  }
}

// Iniciar o envio de logs aleatórios a cada 10 segundos
setInterval(sendRandomLogs, 10000);

// Iniciar o servidor
app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`);
});